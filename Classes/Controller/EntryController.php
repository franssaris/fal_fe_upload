<?php
namespace Unikka\FalFeUpload\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2013 Markus Günther <info@unikka.de>, unikka
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\DebugUtility;

/**
 *
 *
 * @package fal_fe_upload
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class EntryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * entryRepository
	 *
	 * @var \Unikka\FalFeUpload\Domain\Repository\EntryRepository
	 * @inject
	 */
	protected $entryRepository;

	/**
	 * entryRepository
	 *
	 * @var \Unikka\FalFeUpload\Domain\Repository\FileRepository
	 * @inject
	 */
	protected $fileRepository;

	/**
	 * action upload
	 *
	 * @param \Unikka\FalFeUpload\Domain\Model\Entry $newEntry
	 * @return void
	 */
	public function uploadAction(\Unikka\FalFeUpload\Domain\Model\Entry $newEntry = NULL) {
		$this->view->assign('newEntry', $newEntry);
	}

	/**
	 * action create
	 *
	 * @param \Unikka\FalFeUpload\Domain\Model\Entry $newEntry
	 * @return void
	 */
	public function createAction(\Unikka\FalFeUpload\Domain\Model\Entry $newEntry) {

		if (!empty($_FILES['tx_falfeupload_upload'])) {

			/** @var \TYPO3\CMS\Core\Resource\StorageRepository $storageRepository */
			$storageRepository = $this->objectManager->get('TYPO3\CMS\Core\Resource\StorageRepository');
			/** @var \TYPO3\CMS\Core\Resource\ResourceStorage $storage */
			$storage = $storageRepository->findByUid('1');

			for ($index = 0; $index < count($_FILES['tx_falfeupload_upload']['name']['file']); $index++) {
				// setting up file data
				$fileData = array();
				$fileData['name'] = $_FILES['tx_falfeupload_upload']['name']['file'][$index];
				$fileData['type'] = $_FILES['tx_falfeupload_upload']['type']['file'][$index];
				$fileData['tmp_name'] = $_FILES['tx_falfeupload_upload']['tmp_name']['file'][$index];
				$fileData['size'] = $_FILES['tx_falfeupload_upload']['size']['file'][$index];

				if ($fileData['name']) {
					// this will already handle the moving of the file to the storage:
					$newFileObject = $storage->addFile(
						$fileData['tmp_name'],
						$storage->getRootLevelFolder(),
						$fileData['name']
					);
					$newFileObject = $storage->getFile($newFileObject->getIdentifier());
					$newFile = $this->fileRepository->findByUid($newFileObject->getProperty('uid'));

					/** @var \Unikka\FalFeUpload\Domain\Model\FileReference $newFileReference */
					$newFileReference = $this->objectManager->get('Unikka\FalFeUpload\Domain\Model\FileReference');
					$newFileReference->setFile($newFile);

					$newEntry->addFile($newFileReference);
//
					$this->flashMessageContainer->add('Uploade file '.$newFileObject->getName());
				}
			}

			$this->entryRepository->add($newEntry);

			$this->redirect('upload');
		}

	}

	/**
	 * @return \TYPO3\CMS\Core\Resource\ResourceFactory
	 */
	protected function getResourceFactory() {
		return \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();;
	}

}
?>