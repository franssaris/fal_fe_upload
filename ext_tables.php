<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Upload',
	'LLL:EXT:fal_fe_upload/Resources/Private/Language/locallang_db.xlf:falfeupload_plugin.title'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'FAL Upload');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_falfeupload_domain_model_entry', 'EXT:fal_fe_upload/Resources/Private/Language/locallang_csh_tx_falfeupload_domain_model_entry.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_falfeupload_domain_model_entry');
