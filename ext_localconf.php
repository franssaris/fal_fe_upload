<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Unikka.' . $_EXTKEY,
	'Upload',
	array(
		'Entry' => 'upload,create',
		
	),
	// non-cacheable actions
	array(
		'Entry' => 'upload,create',
		
	)
);

?>